class Mapa{

    //Atributos 

    miVisor;
    mapaBase;
    posicionInicial;
    escalaInicial;
    proveedorURL;
    atributosProveedor;
    marcadores=[];
    circulos=[];
    poligonos=[];

    constructor(){

        this.posicionInicial=[4.622906,-74.113385];
        this.escalaInicial=14;

    //Proveedor URL de mapas vectoriales   
        this.proveedorURL='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        
    //Atributos del proveedor    
        this.atributosProveedor={
            maxZoom:20,
        };


        this.miVisor=L.map("mapid");
        this.miVisor.setView(this.posicionInicial,this.escalaInicial);
        this.mapaBase=L.tileLayer(this.proveedorURL,this.atributosProveedor);
        this.mapaBase.addTo(this.miVisor);
    }


    colocarMarcador(posicion){

        this.marcadores.push(L.marker(posicion));
        this.marcadores[this.marcadores.length-1].addTo(this.miVisor).bindPopup("Soy un marcador.");
    }

    colocarCirculo(posicion, configuracion){

        this.circulos.push(L.circle(posicion, configuracion));
        this.circulos[this.circulos.length-1].addTo(this.miVisor);

    }

    colocarPoligono(posicion, configuracion){

        this.poligonos.push(L.polygon(posicion, configuracion));
        this.poligonos[this.poligonos.length-1].addTo(this.miVisor);

    }

}

let miMapa=new Mapa();

//Metodo generar marcador 
miMapa.colocarMarcador([4.631397,-74.114760]);
//Metodo generar circunferencia
miMapa.colocarCirculo([4.631397,-74.114760], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 400
});

miMapa.colocarMarcador([4.622906,-74.113385]);
miMapa.colocarCirculo([4.622906,-74.113385], {
    color: 'blue',
    fillColor: 'blue',
    fillOpacity: 0.5,
    radius: 200
});

miMapa.colocarMarcador([4.628090,-74.065304]);
miMapa.colocarCirculo([4.628090,-74.065304], {
    color: 'yellow',
    fillColor: 'yellow',
    fillOpacity: 0.5,
    radius: 350
});

//Metodo generar marcador
miMapa.colocarMarcador([4.658430,-74.093458]);
//Metodo generar poligono
miMapa.colocarPoligono([[4.664965,-74.093045],
                       [4.656560,-74.098992],
                       [4.652495,-74.093556],
                       [4.659498,-74.089166]]);

miMapa.colocarMarcador([4.701274,-74.146788]);
miMapa.colocarPoligono([[4.683139,-74.135799],
                       [4.683194,-74.135938],
                       [4.697466,-74.154762],
                       [4.697773,-74.154685],
                       [4.705694, -74.165182],
                       [4.705007,-74.170169],
                       [4.710777,-74.171977],
                       [4.720174,-74.159913],
                       [4.717472, -74.156459],
                       [4.695711, -74.125789]]);                       